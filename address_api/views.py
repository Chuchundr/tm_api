from rest_framework import viewsets
from address_api import serializers
from address import models

class CountryViewSet(viewsets.ModelViewSet):
    queryset = models.Country.objects.all().only('uuid', 'country')
    serializer_class = serializers.CountrySerializer


class RegionViewSet(viewsets.ModelViewSet):
    queryset = models.Region.objects.all().only('uuid', 'parent__uuid', 'region')
    serializer_class = serializers.RegionSerializer


class CityViewSet(viewsets.ModelViewSet):
    queryset = models.City.objects.all().only('uuid', 'parent__uuid', 'city')
    serializer_class = serializers.CitySerializer


class AreaViewSet(viewsets.ModelViewSet):
    queryset = models.Area.objects.all().only('uuid', 'parent__uuid', 'area')
    serializer_class = serializers.AreaSerializer


class StreetViewSet(viewsets.ModelViewSet):
    queryset = models.Street.objects.all().only('uuid', 'parent__uuid', 'street')
    serializer_class = serializers.StreetSerializer


class AddressViewSet(viewsets.ModelViewSet):
    queryset = models.Address.objects.all().only('uuid', 'country', 'region', 'city', 'area', 'street', 'house', 'flat', 'index', 'slug', 'lat', 'lng')
    serializer_class = serializers.AddressSerializer

