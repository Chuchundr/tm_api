from rest_framework import serializers
from address import models


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Country
        fields = ['uuid', 'country']


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Region
        fields = ['uuid', 'parent', 'region']

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.City
        fields = ['uuid', 'parent', 'city']

class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Area
        fields = ['uuid', 'parent', 'area']

class StreetSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Street
        fields = ['uuid', 'parent', 'street']

class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Address
        fields = ['uuid',
                  'country',
                  'region',
                  'city',
                  'area',
                  'street',
                  'house',
                  'flat',
                  'index',
                  'slug',
                  'lat',
                  'lng']
