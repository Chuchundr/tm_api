from rest_framework import viewsets
from stores_api import serializers
from stores import models


class StoreViewSet(viewsets.ModelViewSet):
    queryset = models.Store.objects.all().only('name', 'city__uuid')
    serializer_class = serializers.StoreSerializer
