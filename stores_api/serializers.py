from rest_framework import serializers
from stores import models


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Store
        fields = ['uuid', 'name', 'city']
