from django.apps import AppConfig


class StoresApiConfig(AppConfig):
    name = 'stores_api'
