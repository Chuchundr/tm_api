from rest_framework import serializers
from clients import models
from django.contrib.auth.models import User, Group


class ContactsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Contacts
        fields = [
            'uuid',
            'phone_number1',
            'phone_number2',
            'email',
            'slug'
        ]


class PersonalDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PersonalData
        fields = [
            'uuid',
            'first_name',
            'last_name',
            'middle_name',
        ]


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['name']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password']


class UserProfileSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.username')
    group = serializers.CharField(source='group.name')
    
    class Meta:
        model = models.UserProfile
        fields = [
            'uuid',
            'user',
            'bonus_card',
            'contacts',
            'personal_data',
            'basket',
            'group'
        ]

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        username = User.objects.get(username=user_data['username'])
        group_name = validated_data.pop('group')
        group = Group.objects.get(name=group_name['name'])
        profile = models.UserProfile.objects.update_or_create(user=username, group=group, **validated_data)
        return profile[0]
