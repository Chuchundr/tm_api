from rest_framework import viewsets
from django.contrib.auth.models import User, Group
from clients_api import serializers
from clients import models


class ContactsViewSet(viewsets.ModelViewSet):
    queryset = models.Contacts.objects.all().only(
        'uuid',
        'phone_number1',
        'phone_number2',
        'email',
        'slug'
    )
    serializer_class = serializers.ContactsSerializer


class PersonalDataViewSet(viewsets.ModelViewSet):
    queryset = models.PersonalData.objects.all().only(
        'uuid',
        'first_name',
        'last_name',
        'middle_name',
    )
    serializer_class = serializers.PersonalDataSerializer


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.UserSerializer
    lookup_field = 'username'
    
    def get_queryset(self):
        queryset = User.objects.all().only('username', 'password')
        username = self.request.query_params.get('username', None)
        if username is not None:
            queryset = queryset.filter(username=username)
        return queryset


class UserProfileViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.UserProfileSerializer
    queryset = models.UserProfile.objects.all().only(
        'uuid',
        'user',
        'bonus_card',
        'contacts',
        'personal_data',
        'basket',
        'group'
    )


class GroupViewSet(viewsets.ViewSet):
    serializer_class = serializers.GroupSerializer
    lookup_field = 'name'

    def get_queryset(self):
        queryset = Group.objects.all().only('name')
        group = self.request.query_params.get('name', None)
        if group is not None:
            queryset = queryset.filter(group=group)
        return queryset
