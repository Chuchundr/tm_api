from rest_framework import serializers
from marketing import models


class BonusCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BonusCard
        fields = [
            'uuid',
            'name',
            'percent',
            'bonus',
            'limit'
        ]
