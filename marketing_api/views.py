from rest_framework import viewsets
from marketing_api import serializers
from marketing import models


class BonusCardViewSet(viewsets.ModelViewSet):
    queryset = models.BonusCard.objects.all().only(
        'uuid',
        'name',
        'percent',
        'bonus',
        'limit'
    )
    serializer_class = serializers.BonusCardSerializer
