from django.apps import AppConfig


class MarketingApiConfig(AppConfig):
    name = 'marketing_api'
