from django.apps import AppConfig


class PriceApiConfig(AppConfig):
    name = 'price_api'
