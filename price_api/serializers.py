from rest_framework import serializers
from price import models


class CreditTimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.KreditTime
        fields = ['uuid', 'value']


class PricingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Pricing
        fields = [
            'uuid',
            'price',
            'bonus_card',
            'kredit_time',
            'prepayment',
            'monthly'
        ]
