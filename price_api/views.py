from rest_framework import viewsets
from price_api import serializers
from price import models


class CreditTimeViewSet(viewsets.ModelViewSet):
    queryset = models.KreditTime.objects.all().only('uuid', 'value')
    serializer_class = serializers.CreditTimeSerializer


class PricingViewSet(viewsets.ModelViewSet):
    queryset = models.Pricing.objects.all().only(
        'uuid',
        'price',
        'bonus_card__uuid',
        'kredit_time__uuid',
        'prepayment',
        'monthly'
    )
    serializer_class = serializers.PricingSerializer
