from rest_framework import viewsets
from goods_api import serializers
from goods import models


class CategoriesViewset(viewsets.ModelViewSet):
    queryset = models.Categories.objects.all().only('uuid', 'name')
    serializer_class = serializers.CategoriesSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = models.Category.objects.all().only('uuid', 'parent__uuid', 'name')
    serializer_class = serializers.CategorySerializer


class GoodNameViewSet(viewsets.ModelViewSet):
    queryset = models.GoodName.objects.all().only('uuid', 'name')
    serializer_class = serializers.GoodNameSerializer


class GoodViewSet(viewsets.ModelViewSet):
    queryset = models.Good.objects.all().only(
        'uuid', 
        'store__uuid',
        'name__uuid',
        'model',
        'total_price',
        'credit_time',
        'has_prepayment',
        'category__uuid',
        'in_stock',
        'slug',
        'description'
    )
    serializer_class = serializers.GoodSerializer


class SpecificationNameViewSet(viewsets.ModelViewSet):
    queryset = models.SpecificationName.objects.all().only('uuid', 'name')
    serializer_class = serializers.SpecificationNameSerializer


class SpecificationNameBoolViewSet(viewsets.ModelViewSet):
    queryset = models.SpecificationNameBool.objects.all().only('uuid', 'name')
    serializer_class = serializers.SpecificationNameBoolSerializer


class SpecificationValueViewSet(viewsets.ModelViewSet):
    queryset = models.SpecificationValue.objects.all().only('uuid', 'value')
    serializer_class = serializers.SpecificationValueSerializer


class SpecificationViewSet(viewsets.ModelViewSet):
    queryset = models.Specification.objects.all().only(
        'uuid',
        'name__uuid',
        'value__uuid',
        'good__uuid'
    )
    serializer_class = serializers.SpecificationSerializer


class SpecificationBoolViewSet(viewsets.ModelViewSet):
    queryset = models.SpecificationBool.objects.all().only('uuid', 'name', 'value', 'good')
    serializer_class = serializers.SpecificationBoolSerializer
