from rest_framework import serializers
from goods import models


class CategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Categories
        fields = ['uuid', 'name']


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = ['uuid', 'parent', 'name']


class GoodNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GoodName
        fields = ['uuid', 'name']


class GoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Good
        fields = [
            'uuid',
            'store',
            'name',
            'model',
            'total_price',
            'credit_time',
            'has_prepayment',
            'category',
            'in_stock',
            'slug',
            'description'
        ]


class SpecificationNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SpecificationName
        fields = ['uuid', 'name']


class SpecificationNameBoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SpecificationNameBool
        fields = ['uuid', 'name']


class SpecificationValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SpecificationValue
        fields = ['uuid', 'value']


class SpecificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Specification
        fields = ['uuid', 'name', 'value', 'good']


class SpecificationBoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SpecificationBool
        fields = ['uuid', 'name', 'value', 'good']

