from django.apps import AppConfig


class GoodsApiConfig(AppConfig):
    name = 'goods_api'
