from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from django.conf.urls.static import static
from address_api import views as address_views
from clients_api import views as client_views
from goods_api import views as goods_views
from marketing_api import views as marketing_views
#from price_api import views as price_views
from stores_api import views as stores_views
from basket_api import views as basket_views


router = DefaultRouter()

router.register('address/country', address_views.CountryViewSet, basename='country')
router.register('address/region', address_views.RegionViewSet, basename='region')
router.register('address/city', address_views.CityViewSet, basename='city')
router.register('address/area', address_views.AreaViewSet, 'area')
router.register('address/street', address_views.StreetViewSet, basename='street')
router.register('address/address', address_views.AddressViewSet, basename='address')

router.register('clients/user_profile', client_views.UserProfileViewSet, basename='user-profile')
router.register('clients/contacts', client_views.ContactsViewSet, basename='contacts')
router.register('clients/personal', client_views.PersonalDataViewSet, basename='personal')
router.register('clients/user', client_views.UserViewSet, basename='user')
router.register('clients/groups', client_views.GroupViewSet, basename='group')

router.register('basket/basket', basket_views.BasketViewSet, basename='basket')
router.register('basket/basket_item', basket_views.BasketItemViewSet, basename='basket-item')

router.register('goods/categories', goods_views.CategoriesViewset, basename='categories')
router.register('goods/category', goods_views.CategoryViewSet, basename='category')
router.register('goods/good_name', goods_views.GoodNameViewSet, basename='good-name')
router.register('goods/good', goods_views.GoodViewSet, basename='good')
router.register('goods/specification_name', goods_views.SpecificationNameViewSet, basename='specification-name')
router.register('goods/specification_name_bool', goods_views.SpecificationNameBoolViewSet, basename='specification-name-bool')
router.register('goods/specification_value', goods_views.SpecificationValueViewSet, basename='specification-value')
router.register('goods/specification', goods_views.SpecificationViewSet, basename='specification')
router.register('goods/specification_bool', goods_views.SpecificationBoolViewSet, basename='specification-bool')

router.register('marketing/bonus_card', marketing_views.BonusCardViewSet, basename='bonus-card')

#router.register('price/credit_time', price_views.CreditTimeViewSet, basename='creadit-time')
#router.register('price/pricing', price_views.PricingViewSet, basename='pricing')

router.register('stores/store', stores_views.StoreViewSet, basename='store')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ]+urlpatterns
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
