from rest_framework import serializers
from basket import models


class BasketSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Basket
        fields = ['uuid']


class BasketItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BasketItem
        fields = ['uuid', 'basket', 'good', 'number', 'price']
