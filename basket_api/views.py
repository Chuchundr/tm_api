from rest_framework import viewsets
from basket_api import serializers
from basket import models


class BasketViewSet(viewsets.ModelViewSet):
    queryset = models.Basket.objects.all().only('uuid')
    serializer_class = serializers.BasketSerializer


class BasketItemViewSet(viewsets.ModelViewSet):
    queryset = models.BasketItem.objects.all().only(
        'uuid',
        'basket__uuid',
        'good__uuid',
        'number',
        'price'
    )
    serializer_class = serializers.BasketItemSerializer
