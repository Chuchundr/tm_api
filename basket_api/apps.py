from django.apps import AppConfig


class BasketApiConfig(AppConfig):
    name = 'basket_api'
